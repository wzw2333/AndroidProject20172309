package com.example.superwei.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Stack;

public class ThirdActivity extends AppCompatActivity {

    Stack<String> stack = new Stack<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        Toast toast = Toast.makeText(this,"TEST",Toast.LENGTH_LONG);

        Button button1 = (Button)findViewById(R.id.button1);
        button1.setOnClickListener(new myButtonListener1());
        Button button2 = (Button)findViewById(R.id.button2);
        button2.setOnClickListener(new myButtonListener2());

        Toast.makeText(this, "20172309", Toast.LENGTH_SHORT).show();
    }
    public class myButtonListener1 implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            EditText editText1 = (EditText)findViewById(R.id.editText1);
            stack.push(editText1.getText().toString());
            System.out.println("这次添加了: "+editText1.getText().toString());
            EditText editText2=(EditText)findViewById(R.id.editText2);
            editText2.setText(editText1.getText(), TextView.BufferType.EDITABLE);
            editText2.setText(stack.toString(), TextView.BufferType.EDITABLE);
            System.out.println(stack.toString());
        }
    }
    public class myButtonListener2 implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            System.out.println("删去了: "+stack.peek());
            stack.pop();
            EditText editText2=(EditText)findViewById(R.id.editText2);
            editText2.setText(stack.toString(), TextView.BufferType.EDITABLE);
            System.out.println(stack.toString());
        }
    }
}
